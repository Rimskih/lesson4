//
//  DaysOfTheWeek.swift
//  Make!
//
//  Created by Dashiki Rimskih on 10/5/17.
//  Copyright © 2017 Dashiki Rimskih. All rights reserved.
//

import UIKit

class DaysOfTheWeek: NSObject {
    
    static func daysOfTheWeek(displayDay: Int) {
        switch displayDay%7 {
        case 1:
            print("Monday")
        case 2:
            print("Tuesday")
        case 3:
            print("Wednesday")
        case 4:
            print("Thursday")
        case 5:
            print("Friday")
        case 6:
            print("Saturday")
        default:
            print("Sunday")
       
        }
    }
}
