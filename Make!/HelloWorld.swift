//
//  HelloWorld.swift
//  Make!
//
//  Created by Dashiki Rimskih on 10/5/17.
//  Copyright © 2017 Dashiki Rimskih. All rights reserved.
//

import UIKit

class HelloWorld: NSObject {

    static func displayText(countNum: Int) {
        for _ in 0..<countNum {
            print ("Hello World!")
        }
    }
    
}
